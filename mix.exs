defmodule Imgurdl.MixProject do
  use Mix.Project

  def project do
    [
      app: :imgurdl,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      escript: escript(),
      deps: deps(),
      releases: release()
    ]
  end

  def escript do
    []
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {Imgurdl.Application, []}
    ]
  end

  defp deps do
    [
      {:bakeware, "~> 0.2"},
      {:castore, "~> 0.1"},
      {:finch, "~> 0.7"},
      {:flow, "~> 1.1"},
      {:jason, "~> 1.2"},
      {:tesla, "~> 1.4"}
    ]
  end

  defp release do
    [
      imgurdl: [
        steps: [:assemble, &Bakeware.assemble/1]
      ]
    ]
  end
end
