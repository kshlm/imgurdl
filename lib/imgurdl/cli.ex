defmodule Imgurdl.CLI do
  alias Imgurdl.Album

  def main(args) do
    case length(args) do
      2 ->
        Album.download(Enum.at(args, 0), Enum.at(args, 1))

      1 ->
        Album.download(Enum.at(args, 0))

      _ ->
        IO.puts("Usage: imgurdl <ALBUMID> [<DIRECTORY>]")
    end
  end
end
