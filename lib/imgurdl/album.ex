defmodule Imgurdl.Album do
  use Imgurdl
  alias Imgurdl.Dl

  def download(album, dir) do
    Logger.debug("Attempting to download #{album} into #{dir}")

    case album |> get_image_links do
      {:ok, images} ->
        # Create directory
        if not File.dir?(dir) do
          File.mkdir_p!(dir)
        end

        Logger.info("Downloading album #{album} into #{dir}")

        images
        |> Stream.with_index()
        |> Flow.from_enumerable()
        |> Flow.partition()
        |> Flow.map(fn {url, idx} -> dl_image(idx + 1, url, dir) end)
        |> Flow.run()

      :error ->
        Logger.error("Could not download album")
        :error
    end
  end

  def download(album) do
    download(album, album)
  end

  defp dl_image(index, url, dir) do
    filename =
      (Integer.to_string(index) |> String.pad_leading(4, "0")) <> "-" <> Path.basename(url)

    Dl.download(url, Path.join(dir, filename))
  end

  def get_image_links(album_id) do
    case @imgur3 |> get("album/#{album_id}/images") do
      {:ok, %Tesla.Env{status: status, body: %{"data" => images}}} ->
        case status do
          200 ->
            {:ok, images |> Stream.map(&image_link/1)}

          _ ->
            Logger.error("Error getting album #{album_id}: #{status}")
            :error
        end

      {:error, err} ->
        Logger.error("Error getting album #{album_id}: #{err}")
        :error
    end
  end

  defp image_link(image) do
    case image["type"] do
      "image/gif" ->
        if image["mp4_size"] > 0 do
          image["mp4"]
        else
          image["link"]
        end

      _ ->
        image["link"]
    end
  end
end
