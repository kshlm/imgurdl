defmodule Imgurdl.Dl do
  require Logger

  def download(url, filepath) do
    Logger.debug("Downloading #{url} to #{filepath}")

    case Tesla.get(url) do
      {:ok, %Tesla.Env{status: status, body: body}} ->
        case status do
          200 ->
            Logger.debug("Downloaded #{url}")

            case File.write(filepath, body) do
              :ok ->
                Logger.info("Downloaded #{url} to #{filepath}")
                :ok

              {:error, errno} ->
                Logger.error("Failed to write #{url} to #{filepath}: #{errno}")
                :error
            end

          _ ->
            Logger.error("Downloading #{url} failed: #{status}")
            :error
        end

      {:error, err} ->
        Logger.error("Downloading #{url} failed: #{err}")
        :error
    end
  end
end
