defmodule Imgurdl do
  defmacro __using__(_) do
    quote do
      require Logger

      use Tesla

      @api_urlbase "https://api.imgur.com/3/"
      @client_id Application.get_env(:imgurdl, :client_id)
      @imgur3 Tesla.client([
                {Tesla.Middleware.BaseUrl, @api_urlbase},
                {Tesla.Middleware.Headers, [{"Authorization", "Client-ID #{@client_id}"}]},
                Tesla.Middleware.JSON
              ])
    end
  end
end
